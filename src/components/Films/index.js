import React from 'react';
import { Link } from 'react-router-dom';

import { FilmsList } from '../../data/films.js'

import '../../styles/styles.css'

export default function Films(props) {

  let films = FilmsList.map((film) => {
    return (
      <div className="actor-container">
        <a href="#" alt="best films oscar 2019">
        <div className="actor-image" style={{ backgroundImage: "url(" + film.img_src + ")" }}></div>
        </a>
        <h3>{film.name}</h3>
      </div>
    )
  })

  return (
    <div className="main-content">
      <div><Link className="back" to="/">Back</Link></div>
      <h2>{props.title}</h2>
      <div className="container">
        {films}
      </div>
    </div>
  );
}