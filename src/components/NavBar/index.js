import React from 'react';
import { NavLink } from 'react-router-dom';

const NavBar = (props) => (
  <nav>
    <h2 className="logo"><a className="logo-link" href="/">{props.title}</a></h2>
    <ul className="nav-menu">
      <li><NavLink className="link-active" activeClassName="activate" exact to="/">Home</NavLink></li>
      <li><NavLink className="link-active" activeClassName="activate" to="/actors">Best Actors</NavLink></li>
      <li><NavLink className="link-active" activeClassName="activate" to="/actresses">Best Actress</NavLink></li>
      <li><NavLink className="link-active" activeClassName="activate" to="/films">Best Films</NavLink></li>
    </ul>
  </nav>
);

export default NavBar;