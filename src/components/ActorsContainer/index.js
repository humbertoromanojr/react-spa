import React from 'react';
import { Route } from 'react-router-dom';

import Actor from '../Actor';
import Actors from '../Actors';
import { ActorList } from '../../data/actors';

export default function ActorsContainer() {

  let actorUrl = ActorList.map((actor) => {
    return (
      <Route path={`/actors/${actor.url}`} render={() => <Actor name={actor.name} image={actor.profile_img} details={actor.description} />} />
    );
  })

  return (
    <>
      <Route exact path="/actors" render={() => <Actors title="Best Actors" />} />
      {actorUrl}
    </>
  );
}
