export const ActressList = [
  {
      name: "Olivia Colman",
      description: "Sarah Caroline Olivia Sinclair, CBE (née Colman; 30 January 1974), better known as Olivia Colman, is an English actress. She is the recipient of numerous accolades, including an Academy Award, three Golden Globe Awards, four BAFTA Awards, four British Independent Film Awards, a Screen Actors Guild Award, a Volpi Cup, and a BFI Fellowship.",
      img_src:"https://cdnph.upi.com/svc/sv/upi/2341536494065/2018/1/7fd1f1bf9e3ab12e957fd4cbc431746b/Roma-Olivia-Colman-and-Willem-Dafoe-win-top-awards-in-Venice.jpg",
      profile_img: "https://abrilclaudia.files.wordpress.com/2018/07/gettyimages-869457670.jpg?quality=85&strip=info&w=1024&h=1536",
      url: "olivia-colman"
  },
  {
      name: "Glenn Close",
      description: "(born March 19, 1947) is an American actress, singer, and producer. Regarded as one of the greatest actresses of her generation,[1] she is the recipient of numerous accolades, including three Primetime Emmy Awards, three Tony Awards, and three Golden Globe Awards. A seven-time Academy Award nominee, she holds the record as the actress to have the most nominations without a win. In 2016, Close was inducted into the American Theater Hall of Fame, and in 2019, Time magazine named her one of the 100 most influential people in the world.",
      img_src: "https://pbs.twimg.com/profile_images/1024493006240534528/maoAzOZq_400x400.jpg",
      profile_img: "https://queridojeito.com/wp-content/uploads/2016/06/Glenn-Close.jpg",
      url: "glenn-close"
      
  },
  {
    name: "Lady Gaga",
    description: "Stefani Joanne Angelina Germanotta (born March 28, 1986), known professionally as Lady Gaga, is an American singer, songwriter, and actress. She is known for reinventing herself throughout her career and for her versatility in numerous areas of the entertainment industry. Gaga began performing as a teenager, singing at open mic nights and acting in school plays. She studied at Collaborative Arts Project 21, through New York University's Tisch School of the Arts, before dropping out to pursue a music career. When Def Jam Recordings canceled her contract, she worked as a songwriter for Sony/ATV Music Publishing, where Akon helped her sign a joint deal with Interscope Records and his own label KonLive Distribution in 2007.",
    img_src: "https://pixel.nymag.com/imgs/daily/vulture/2018/12/10/10-lady-gaga.w330.h330.jpg",
    profile_img: "https://correio-cdn1.cworks.cloud/fileadmin/_processed_/4/5/csm_lady2_bd712a84a1.jpg",
    url: "lady-gaga"
  },
  {
    name: "Yalitza Aparicio",
    description: " (Spanish: (About this soundlisten); born 11 December 1993)) is a Mexican actress who made her film debut as Cleo in Alfonso Cuarón's 2018 drama Roma, which earned her a nomination for the Academy Award for Best Actress. Time magazine named her one of the 100 most influential people in the world in 2019.[1] On October 4, 2019 she was named UNESCO Goodwill Ambassador for Indigenous Peoples.",
    img_src: "https://tomandlorenzo.com/wp-content/uploads/2019/01/Yalitza-Aparicio-Roma-Critics-Choice-Awards-2019-Red-Carpet-Fashion-Prada-Tom-Lorenzo-Site-5.jpg",
    profile_img: "https://www.wellesley.edu/sites/default/files/styles/event2/public/assets/departments/events/images/romarr-1.jpg?itok=mgHMBoFC",
    url: "yalitza-aparicio"
  },
  {
    name: "Melissa McCarthy",
    description: "Melissa Ann McCarthy (born August 26, 1970)[1] is an American actress, comedian, writer, producer, and fashion designer. She is the recipient of numerous accolades, including two Primetime Emmy Awards and has received nominations for two Academy Awards and two Golden Globe Awards. McCarthy was named by Time as one of the 100 most influential people in the world in 2016, and she has been featured multiple times in annual rankings of the highest-paid actresses in the world.",
    img_src: "https://www.emmys.com/sites/default/files/styles/bio_pics_detail/public/melissa-mccarthy-450x600.jpg?itok=9LAVsNDz",
    profile_img: "https://i0.wp.com/pipocamoderna.com.br/wp-content/uploads/2017/02/melissa-mccarthy.jpg",
    url: "melissa-mccarthy",
  }
];
  