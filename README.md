<h1 align="center">
<br>
  <img src="https://i.ibb.co/dkN4rmm/react-sap.png" alt="react-sap" width="320" border="0">
<br>
</h1>


# SPA - Single Page Application

This application was for my training to learn how to make a one page application, more called SPA.

## Technologies

- ⚛️ **React Js** — A JavaScript library for building user interfaces

-   [ReactJS](https://reactjs.org/)
-   [React Router](https://github.com/ReactTraining/react-router)


## Sources
- https://www.youtube.com/watch?v=QL12oC-AvBA&t=1276s


## Installation

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`npm or yarn install`

`npm or yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## License

This project is licensed under the MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) page for details.

## Demonstration 
<p align="center">
<br>
  <img src="https://i.ibb.co/zFTZTFx/spa-web-mall.gif" alt="spa-web-mall" border="0">
<br>
</p>

